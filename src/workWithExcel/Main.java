package workWithExcel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        createExcelFile();
//        readFromExcel();
//        workWithFormulas();
//        workWithCell();
    }

    private static void createExcelFile() throws IOException {
        Workbook wb = new XSSFWorkbook();  //Создание книги Excel xsl
        Sheet sheet0 = wb.createSheet("Издатели"); //Создание листа
        Sheet sheet1 = wb.createSheet("Произведения");

        Row row = sheet0.createRow(0); //Создание строки, начало с 0
        Cell cell = row.createCell(0); //Создание ячейки, начало с 0
        cell.setCellValue("O'Reilly"); //Запись данных в ячейку

        FileOutputStream outputStream = new FileOutputStream("my.xlsx"); //Запись книги в файл
        wb.write(outputStream);
        outputStream.close();
    }

    private static void readFromExcel() throws IOException {
        FileInputStream inputStream = new FileInputStream("my.xls");
        Workbook wb = new HSSFWorkbook(inputStream); //Создаем книгу и указываем откуда читать
        String result = wb.getSheetAt(0).getRow(0).getCell(0).getStringCellValue(); //Чтение данных из листа/строки/ячейки
        System.out.println(result);

        for (Row row : wb.getSheetAt(0)) { //Проход по всем ячейкам листа
            for (Cell cell : row) {
                CellReference reference = new CellReference(row.getRowNum(), cell.getColumnIndex()); //Индекс ячейки
                System.out.println(reference.formatAsString() + " - " + cell.getStringCellValue());
            }
        }

        inputStream.close();
    }

    private static void workWithFormulas() throws IOException {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet0 = wb.createSheet("Формула");
        Row row = sheet0.createRow(0);
        Cell cell0 = row.createCell(0);
        cell0.setCellValue(5);
        Cell cell1 = row.createCell(1);
        cell1.setCellValue(7);

        Cell cell3 = row.createCell(2);
        cell3.setCellFormula("A1 + B1");  //Запись формулы

        FileOutputStream outputStream = new FileOutputStream("my.xls");
        wb.write(outputStream);
        outputStream.close();
    }

    private static void workWithCell() throws IOException {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet0 = wb.createSheet("Форматирование");
        Row row = sheet0.createRow(0);
        Cell cell0 = row.createCell(0);
        cell0.setCellValue("Хаюшки");

        CellStyle cellStyle = wb.createCellStyle(); //Создание объекта стиля
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND); //Создание типа кисти
        cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex()); //Установка цвета фона
        cellStyle.setAlignment(HorizontalAlignment.CENTER); //Выравнивание
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setBorderBottom(BorderStyle.DASH_DOT); //Изменение границы
        cellStyle.setBottomBorderColor(IndexedColors.DARK_TEAL.getIndex());

        Font font = wb.createFont(); //Создание объекта шрифт
        font.setFontName("Courier new"); //Тип шрифта
        font.setFontHeightInPoints((short) 14); //Кегель
        font.setBold(true); //Жирный
        font.setStrikeout(true); //Зачеркнутый
        font.setUnderline(Font.U_SINGLE); //Одинарное подчеркивание
        font.setColor(IndexedColors.BRIGHT_GREEN.getIndex()); //Цвет шрифта
        cellStyle.setFont(font);

        cell0.setCellStyle(cellStyle);

        sheet0.setColumnWidth(0, 3000); //Ширина столбца
        sheet0.autoSizeColumn(0);
        row.setHeightInPoints(35); //Высота столбца
        sheet0.addMergedRegion(new CellRangeAddress(0, 3, 0, 3)); //Объединение ячеек

        FileOutputStream outputStream = new FileOutputStream("my.xls");
        wb.write(outputStream);
        outputStream.close();
    }
}
