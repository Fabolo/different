package savePicture;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("src/savePicture/animal.png");
        BufferedImage image = ImageIO.read(file);
        System.out.println(image);

        ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
        ImageIO.write(image, "png", baos);
        baos.flush();
        String base64String = Base64.encode(baos.toByteArray());
        baos.close();
        byte[] resByteArray = Base64.decode(base64String);
        BufferedImage resultImage = ImageIO.read(new ByteArrayInputStream(resByteArray));
        ImageIO.write(resultImage, "png", new File("src/savePicture/","resultImage.png"));
    }
}
