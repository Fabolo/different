package CommandLineCalendar.dao.mock;

import CommandLineCalendar.been.MyMonth;

import java.time.Month;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DataSource {
    private Calendar calendar = Calendar.getInstance();
    private Map<Integer, MyMonth> months = new HashMap<>();

    public Calendar getCalendar() {
        return calendar;
    }

    public MyMonth getMonth(int number) {
        MyMonth month;
        if (months.containsKey(number)) month = months.get(number);
        else {
            calendar.set(2017, number - 1, 1);
            int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            String startNameDay = String.format(Locale.ENGLISH, "%ta", calendar.getTime());
            month = new MyMonth(Month.of(number), days, startNameDay);
            months.put(number, month);
        }
        return month;
    }
}
