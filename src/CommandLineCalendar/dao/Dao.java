package CommandLineCalendar.dao;

import CommandLineCalendar.been.MyMonth;
import CommandLineCalendar.dao.mock.DataSource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Dao {
    private DataSource dataSource = new DataSource();

    public String getCurrentDataAndTime() {
        Calendar calendar = dataSource.getCalendar();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);
        StringBuffer buffer = new StringBuffer();

        String data = String.format("%1$s %4$s %2$td%3$s %2$tY\n", "Today is:", calendar, ",", dateFormat.format(calendar.getTime()));
        String time = String.format("Time: %tT \n", calendar);
        buffer.append(data).append(time);

        return String.valueOf(buffer);
    }

    public MyMonth getMonth(int number) {
        if (number < 1 || number > 12) throw new IllegalArgumentException("There is no month at number " + number);
        return dataSource.getMonth(number);
    }
}
