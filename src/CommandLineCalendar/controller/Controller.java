package CommandLineCalendar.controller;

import CommandLineCalendar.model.Model;
import CommandLineCalendar.view.MainView;
import CommandLineCalendar.view.MonthView;

public class Controller {
    private Model model;
    private MainView mainView;
    private MonthView monthView;

    public void setModel(Model model) {
        this.model = model;
    }

    public void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public void setMonthView(MonthView monthView) {
        this.monthView = monthView;
    }

    public void onShowStartDialog() {
        model.startDialog();
        mainView.refresh(model.getModelData());
    }

    public void onShowCurrentMonth(int number) {
        model.showCurrentMonth(number);
        monthView.refresh(model.getModelData());
    }

    public void onAddEvent(int day, String name, String details) {
        model.addEvent(day, name, details);
        monthView.refresh(model.getModelData());
    }

    public void onShowAllEvents() {
        model.showEvents();
        mainView.refresh(model.getModelData());
    }

    public void onShowCurrentEvents(int number) {
        model.showEventsInMonth(number);
        mainView.refresh(model.getModelData());
    }

    public void onChangeCurrentEvent(int day, int month, String name, String details) {
        model.changeCurrentEvent(day, month, name, details);
        mainView.refresh(model.getModelData());
    }

    public void onDeleteCurrentEvent(int day, int month) {
        model.deleteCurrentEvent(day, month);
        mainView.refresh(model.getModelData());
    }
}
