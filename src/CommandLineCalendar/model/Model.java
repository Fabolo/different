package CommandLineCalendar.model;

public interface Model {
    ModelData getModelData();
    void startDialog();
    void showCurrentMonth(int number);
    void addEvent(int day, String name, String details);
    void showEvents();
    void showEventsInMonth(int number);
    void changeCurrentEvent(int day, int month, String name, String details);
    void deleteCurrentEvent(int day, int month);
}
