package CommandLineCalendar.model;

import CommandLineCalendar.model.service.UserService;

public class MainModel implements Model {
    private ModelData model= new ModelData();
    private UserService userService = new UserService();
    private int lastUseMonth;

    @Override
    public ModelData getModelData() {
        return model;
    }

    @Override
    public void startDialog() {
        model.setDialog(userService.getStartDialog());
    }

    @Override
    public void showCurrentMonth(int number) {
        model.setMonth(userService.getMonth(number));
        lastUseMonth = number;
    }

    @Override
    public void addEvent(int day, String name, String details) {
        model.setMonth(userService.addEvent(lastUseMonth, day, name, details));
    }

    @Override
    public void showEvents() {
        model.setDialog(userService.getAllEvents());
    }

    @Override
    public void showEventsInMonth(int number) {
        model.setDialog(userService.getEventsInMonth(number));
    }

    @Override
    public void changeCurrentEvent(int day, int month, String name, String details) {
        model.setDialog(userService.changeEvent(day, month, name, details));
    }

    @Override
    public void deleteCurrentEvent(int day, int month) {
        model.setDialog(userService.deleteEvent(day, month));
    }
}
