package CommandLineCalendar.model;

import CommandLineCalendar.been.MyMonth;

public class ModelData {
    private StringBuilder builder;
    private MyMonth month;

    public StringBuilder getDialog() {
        return builder;
    }

    public MyMonth getMonth() {
        return month;
    }

    void setDialog(StringBuilder builder) {
        this.builder = builder;
    }

    void setMonth(MyMonth month) {
        this.month = month;
    }
}
