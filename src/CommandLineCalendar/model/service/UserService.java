package CommandLineCalendar.model.service;

import CommandLineCalendar.been.MyMonth;
import CommandLineCalendar.dao.Dao;

public class UserService {
    private Dao dao = new Dao();

    public MyMonth getMonth(int number) {
        return dao.getMonth(number);
    }

    public MyMonth addEvent(int month, int day, String name, String details) {
        MyMonth currentMonth = getMonth(month);
        checkDaysInMonth(currentMonth, day);
        currentMonth.createEvent(day, name, details);
        return currentMonth;
    }

    public StringBuilder getAllEvents() {
        StringBuilder allEvents = new StringBuilder();
        for (int i = 1; i < 13; i++) {
            allEvents.append(getEventsInMonth(i));
            if (i != 12) allEvents.append("\n").append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~").append("\n");
        }
        return allEvents;
    }

    public StringBuilder getEventsInMonth(int number) {
        StringBuilder events = new StringBuilder();
        MyMonth currentMonth = getMonth(number);
        if (currentMonth.isEvents()) events.append(currentMonth.getAllEvents());
        if (events.toString().equals("")) events.append("There are no events in ").append(currentMonth.getName().toLowerCase());
        return events;
    }

    public StringBuilder changeEvent(int day, int month, String name, String details) {
        MyMonth currentMonth = getMonth(month);
        checkDaysInMonth(currentMonth, day);
        return currentMonth.changeEvent(day, name, details);
    }

    public StringBuilder deleteEvent(int day, int month) {
        MyMonth currentMonth = getMonth(month);
        checkDaysInMonth(currentMonth, day);
        return currentMonth.deleteEvent(day);
    }

    private void checkDaysInMonth(MyMonth currentMonth, int day) {
        if (day < 1 || day > currentMonth.getCountDays()) throw new IllegalArgumentException("There is no day at number " + day + " in " + currentMonth.getName());
    }

    public StringBuilder getStartDialog() {
        StringBuilder result = new StringBuilder();
        result.append("Calendar is start working...\n").append(dao.getCurrentDataAndTime()).append("\n")
                .append("1. View a specific month of the current year (see).\n")
                .append("2. See event (event)).\n")
                .append("3. Add an event (add)).\n")
                .append("4. Change the event (change).\n")
                .append("5. Delete the event (delete).\n")
                .append("6. Finish the program (exit).\n")
                .append("What would you like to do?");
        return result;
    }
}
