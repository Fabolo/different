package CommandLineCalendar.been;

import java.time.Month;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MyMonth {
    private String name;
    private int countDays;
    private String startDays;
    private String[][] days = new String[7][7];
    private Map<Integer, String> events = new HashMap<>();

    public MyMonth(Month name, int countDays, String startDays) {
        this.name = name.toString();
        this.countDays = countDays;
        this.startDays = startDays;
        setDays();
    }

    @Override
    public String toString() {
        return "Name = '" + name + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyMonth month = (MyMonth) o;
        return countDays == month.countDays && (name != null ? name.equals(month.name) : month.name == null) && (startDays != null ? startDays.equals(month.startDays) : month.startDays == null) && Arrays.deepEquals(days, month.days) && (events != null ? events.equals(month.events) : month.events == null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + countDays;
        result = 31 * result + (startDays != null ? startDays.hashCode() : 0);
        result = 31 * result + Arrays.deepHashCode(days);
        result = 31 * result + (events != null ? events.hashCode() : 0);
        return result;
    }

    public int getCountDays() {
        return countDays;
    }

    public String getName() {
        return name.toLowerCase();
    }

    public void createEvent(int numberDay, String nameEvent, String event) {
        for (int i = 1; i < days.length; i++) {
            for (int j = 0; j < days[0].length; j++) {
                if (days[i][j].equals(String.valueOf(numberDay))) days[i][j] = days[i][j] + "*";
            }
        }

        if (!events.containsKey(numberDay)) events.put(numberDay, nameEvent + ": " + event);
        else events.put(numberDay, events.get(numberDay) + "\n" + nameEvent + ": " + event);

    }

    public StringBuilder getAllEvents() {
        StringBuilder allEvents = new StringBuilder();
        allEvents.append(this).append("\n");

        for (Map.Entry<Integer, String> entry : events.entrySet()) {
            allEvents.append("Events for the ").append(entry.getKey()).append(" number:");
            allEvents.append("\n").append(entry.getValue());
        }
        return allEvents;
    }

    public boolean isEvents() {
        return !events.isEmpty();
    }

    public StringBuilder changeEvent(int day, String nameEvent, String detailsEvent) {
        StringBuilder result = new StringBuilder();

        if (events.containsKey(day)) {
            result.append("Replace: ").append(events.get(day)).append("\n");
            if (!nameEvent.equals("") && !detailsEvent.equals("")) {
                result.append("into: ").append(nameEvent).append(": ").append(detailsEvent);
                events.replace(day, nameEvent + ": " + detailsEvent);
            }
            else if (nameEvent.equals("") && !detailsEvent.equals("")) {
                String[] words = events.get(day).split(":");
                words[1] = " " + detailsEvent;
                result.append("into: ").append(words[0]).append(": ").append(detailsEvent);
                events.replace(day, words[0] + ":" + words[1]);
            } else if (!nameEvent.equals("") && detailsEvent.equals("")){
                String[] words = events.get(day).split(":");
                words[0] = nameEvent;
                result.append("into: ").append(nameEvent).append(": ").append(words[1]);
                events.replace(day, words[0] + ": " + words[1]);
            } else result.append("\nThe event has not been changed!");
        } else result.append("There are no events at ").append(day).append(" in ").append(name.toLowerCase());
        return result;
    }

    public StringBuilder deleteEvent(int day) {
        StringBuilder result = new StringBuilder();

        if (events.containsKey(day)) {
            events.remove(day);
            for (int i = 1; i < days.length; i++) {
                for (int j = 0; j < days[0].length; j++) if (days[i][j].contains(day + "*")) days[i][j] = String.valueOf(day);
            }
            result.append("Event(s) successfully deleted!");
        } else result.append("There are no events at ").append(day).append(" in ").append(name.toLowerCase());

        return result;
    }

    public void print() {
        System.out.println(this);
        String[][] days = getDays();
        for (String[] week : days) {
            for (String day : week) System.out.print(day + "\t");
            System.out.println();
        }
    }

    private void setDays() {
        int currentDay = 1;
        int startDay;

        switch (startDays) {
            case ("Mon"): startDay = 0; break;
            case ("Tue"): startDay = 1; break;
            case ("Wed"): startDay = 2; break;
            case ("Thu"): startDay = 3; break;
            case ("Fri"): startDay = 4; break;
            case ("Sat"): startDay = 5; break;
            case ("Sun"): startDay = 6; break;
            default: startDay = 0;
        }

        for (int i = 0; i < startDay; i++) {
            days[1][i] = ".";
        }

        days[0][0] = "Mon";
        days[0][1] = "Tue";
        days[0][2] = "Wed";
        days[0][3] = "Thu";
        days[0][4] = "Fri";
        days[0][5] = "Sat";
        days[0][6] = "Sun";

        for (int i = 1; i < days.length; i++) {
            for (int j = 0; j < days[0].length; j++) {
                if (days[i][j] == null) days[i][j] = currentDay == countDays + 1 ? "." : String.valueOf(currentDay++);
            }
        }
    }

    private String[][] getDays() {
        return days;
    }
}
