package CommandLineCalendar;

import CommandLineCalendar.controller.Controller;
import CommandLineCalendar.model.MainModel;
import CommandLineCalendar.model.Model;
import CommandLineCalendar.view.MainView;
import CommandLineCalendar.view.MonthView;

public class Main {
    public static void main(String[] args) {
        Model model = new MainModel();
        MainView mainView = new MainView();
        MonthView monthView = new MonthView();
        Controller controller = new Controller();

        mainView.setController(controller);
        monthView.setController(controller);

        controller.setModel(model);
        controller.setMainView(mainView);
        controller.setMonthView(monthView);

        mainView.fireEventShowStartDialog();
        mainView.fireEventShowCurrentMonth(10);
        monthView.fireEventAddEvent(10, "St", "Many money!");
        monthView.fireEventAddEvent(10, "Wt", "Little money!");
        mainView.fireEventShowCurrentMonth(7);
        monthView.fireEventAddEvent(18, "Xz", "Conference!");
        mainView.fireEventShowAllEvents();
        mainView.fireEventShowCurrentEvents(7);
        mainView.fireEventChangeCurrentEvent(18, 7, "GT", "New conference!");
        mainView.fireEventDeleteCurrentEvent(18, 7);
        mainView.fireEventShowCurrentMonth(7);
        mainView.fireEventShowCurrentEvents(7);
    }
}
