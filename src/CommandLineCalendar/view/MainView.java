package CommandLineCalendar.view;

import CommandLineCalendar.controller.Controller;
import CommandLineCalendar.model.ModelData;

public class MainView implements View {
    private Controller controller;

    @Override
    public void refresh(ModelData modelData) {
        System.out.println(modelData.getDialog());
        System.out.println("===================================");
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void fireEventShowStartDialog() {
        controller.onShowStartDialog();
    }

    public void fireEventShowCurrentMonth(int numberOfMonth) {
        System.out.println("User: see " + numberOfMonth);
        controller.onShowCurrentMonth(numberOfMonth);
    }

    public void fireEventShowAllEvents() {
        System.out.println("User: event");
        controller.onShowAllEvents();
    }

    public void fireEventShowCurrentEvents(int numberOfMonth) {
        System.out.println("User: event " + numberOfMonth);
        controller.onShowCurrentEvents(numberOfMonth);
    }

    public void fireEventChangeCurrentEvent(int dayOfMonth, int numberOfMonth, String name, String details) {
        System.out.println("User: change " + dayOfMonth + "." + numberOfMonth + "\nname: " + name + "; details: " + details + "\n");
        controller.onChangeCurrentEvent(dayOfMonth, numberOfMonth, name, details);
    }

    public void fireEventDeleteCurrentEvent(int dayOfMonth, int numberOfMonth) {
        System.out.println("User: delete " + dayOfMonth + "." + numberOfMonth);
        controller.onDeleteCurrentEvent(dayOfMonth, numberOfMonth);
    }
}
