package CommandLineCalendar.view;

import CommandLineCalendar.controller.Controller;
import CommandLineCalendar.model.ModelData;

public class MonthView implements View {
    private Controller controller;

    @Override
    public void refresh(ModelData modelData) {
        modelData.getMonth().print();
        System.out.println("===================================");
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void fireEventAddEvent(int dayOfMonth, String name, String details) {
        System.out.printf("User: add %d, %s, %s\n", dayOfMonth , name, details);
        controller.onAddEvent(dayOfMonth, name, details);
    }
}
