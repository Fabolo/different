package CommandLineCalendar.view;

import CommandLineCalendar.controller.Controller;
import CommandLineCalendar.model.ModelData;

public interface View {
    void refresh(ModelData modelData);
    void setController(Controller controller);
}
